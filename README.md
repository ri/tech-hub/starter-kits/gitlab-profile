## Overview

This group acts as a central hub for materials designed to support various EBRAINS processes, including onboarding, project initiation, and adherence to best practices. 

## Purpose

The aim of this group is to streamline the execution of common tasks and workflows within EBRAINS. Whether you're newly joining and in need of onboarding assistance or embarking on a fresh project, this repository is your go-to for essential resources that facilitate a smooth start and efficient progress.

## Structure

- **Mirror GitHub to Gitlab Project:** This section contains resources and guides on how to synchronize projects between GitHub and GitLab, ensuring seamless project management and collaboration across platforms.

## Ownership

The development and upkeep of starter kits found within this group are collaboratively managed by contributors. Specific starter kits might have appointed owners tasked with ensuring the materials remain current and useful.

## Membership Guidelines

The Starter Kits group welcomes anyone who might benefit from the resources it offers. Members are encouraged to delve into the available materials and contribute their expertise or feedback to enrich the repository for all.

## Contacts

For inquiries or further assistance, please reach out to [ngeorgomanolis@athenarc.gr](mailto:ngeorgomanolis@athenarc.gr).
